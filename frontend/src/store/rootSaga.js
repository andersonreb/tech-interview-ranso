import { takeLatest, all } from 'redux-saga/effects';

import { QuizTypes } from '../quiz/redux';
import { AnswerTypes } from '../answer/redux';
import { QuestionTypes } from '../question/redux';

import * as Quiz from '../quiz/sagas';
import * as Answer from '../answer/sagas';
import * as Question from '../question/sagas';



export default function * root() {
    yield all([                

        /** QUIZ **/
        takeLatest(QuizTypes.QUIZ_SAVE, Quiz.save),
        takeLatest(QuizTypes.QUIZ_REQUEST, Quiz.initialRequest),

        /** QUESTION **/
        takeLatest(QuestionTypes.QUESTION_SAVE, Question.save),
        takeLatest(QuestionTypes.QUESTION_REQUEST, Question.initialRequest),

        /** ANSWER **/
        takeLatest(AnswerTypes.ANSWER_REQUEST, Answer.initialRequest), // verificar utilidade
        takeLatest(AnswerTypes.ANSWER_SAVE, Answer.save),
    ]);
};