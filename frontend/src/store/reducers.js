import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';



const rootReducer = combineReducers({
    routing: routerReducer,
    quiz: require('../quiz/redux').reducer,
    answer: require('../answer/redux').reducer,
    question: require('../question/redux').reducer
});

export default rootReducer;