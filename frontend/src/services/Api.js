import axios from 'axios';



export const api = { 

    /** QUIZ **/
    getQuizzes: () => axios.get('http://localhost:3003/quizzes'),
    saveQuiz: (payload) => axios.post('http://localhost:3003/quizzes', payload),

    /** QUESTION **/
    getQuestions: () => axios.get('http://localhost:3003/questions'),
    saveQuestion: (payload) => axios.post('http://localhost:3003/questions', payload),
    getAnswers: () => axios.get('http://localhost:3003/answers'),

    /** ANSWER **/
    saveAnswer: (payload) => axios.post('http://localhost:3003/answers', payload),
    getAnswersByQuestionId: (payload) => axios.get(`http://localhost:3003/answers?questionId=${payload}`),
}