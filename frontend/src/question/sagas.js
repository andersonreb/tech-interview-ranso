import { call, put } from 'redux-saga/effects';

import QuestionActions from './redux';
import { api } from '../services/Api';



export function * initialRequest() {
    try {
        const questionsResponse = yield call(api.getQuestions);
        const answersResponse = yield call(api.getAnswers);
        
        if(questionsResponse.ok === answersResponse.ok) {
            yield put(QuestionActions.questionSuccess({ 
                questions: questionsResponse.data,
                answers: answersResponse.data
            }));
        }
    } catch(e) {
        yield put(QuestionActions.questionFailure({ 
            feedbackMessage: 'Oh no! Something went wrong in request.' 
        }));
    }       
}


export function * save({ question }) {  
    try {
        const response = yield call(api.saveQuestion, question);
        if(response.status === 201) {
            const resultset = yield call(api.getQuestions);
            yield put(QuestionActions.questionSuccess({ 
                questions: resultset.data,
                feedbackMessage: "Successful saved!" 
            }));
        }
    }  catch(e) {
        yield put(QuestionActions.questionFailure({ 
            feedbackMessage: 'Oh no! Something went wrong in save.' 
        }));
    }
}