import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { get } from 'lodash';


const { Types, Creators } = createActions({
    questionSave: ['question'],
    questionRequest: null,
    questionSuccess: ['payload'],
    questionFailure: ['error'],
});

export const QuestionTypes = Types;
export default Creators;


export const INITIAL_STATE = Immutable({
    answers: [],
    question: {}, 
    questions: [],
    isLoading: false,
    errorMessage: null,
    successMessage: null
});


export const request = (state) => state.merge({
    isLoading: true
});

export const success = (state, { payload }) => state.merge({
    answers: get(payload, ['answers'], state.answers),
    question: get(payload, ['question'], state.question),
    questions: get(payload, ['questions'], state.questions), 
    successMessage: get(payload, ['feedbackMessage'], null),
    isLoading: false
});

export const failure = (state, { error }) => ({    
    isLoading: false,
    errorMessage: get(error, 'feedbackMessage', null),
});


export const reducer = createReducer(INITIAL_STATE, {
    [Types.QUESTION_SAVE]: request,
    [Types.QUESTION_REQUEST]: request,
    [Types.QUESTION_SUCCESS]: success,
    [Types.QUESTION_FAILURE]: failure,
});
