import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Card, Button, Spin, Input, Form, Row, Col, message, Select } from 'antd';
import { get, map } from 'lodash';
import uuidv1 from 'uuid/v1';

import QuestionActions from '../redux';
import QuizActions from '../../quiz/redux';



const { Item } = Form;
const { Option } = Select;

class QuestionForm extends Component {

    constructor(props) {
        super(props);
        const { selectedQuiz } = this.props;
        this.state = {
            question: {
                id: uuidv1(),
                description: null,
                quizId: selectedQuiz.id
            }
        };
    }

    handleInputChange = (event, property) => {
        let { question } = this.state; 
        question[property] = event.target.value;
        this.setState({ question });
    }

    handleSelectChange = (event, property) => {
        let { question } = this.state; 
        question[property] = event;
        this.setState({ question });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if(!err) {
                this.setState({ values }, () => this.props.questionSave(this.state.question));
                this.props.form.resetFields();
            }
        });
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        const { isLoading, selectedQuiz, showQuiz } = this.props;

        return (
            <Fragment>
                <Spin spinning={isLoading}>
                    <Card title="Add Questions">
                        <Form onSubmit={this.handleSubmit}>

                            <Row gutter={16}>
                                <Col xs={12} md={12}>
                                    <Item label="Quiz  Theme">
                                        <Select value={selectedQuiz.id} disabled>
                                            <Option 
                                                key={selectedQuiz.id} 
                                                value={selectedQuiz.id} 
                                                children={selectedQuiz.theme} 
                                            />
                                        </Select>
                                    </Item>
                                </Col>
                                <Col xs={12} md={12}>
                                    <Item label="Question">
                                        {getFieldDecorator('description', {
                                            rules: [{ required: true, message: 'Please input the question!' }],
                                        })(
                                            <Input onChange={e => this.handleInputChange(e, 'description')} />
                                        )}
                                    </Item>
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col xs={8} sm={3} md={4} lg={3} xl={2}>
                                    <Button onClick={() => showQuiz()} icon="left">Return</Button>
                                </Col>
                                <Col xs={16} sm={21} md={20} lg={21} xl={22}>
                                    <Button type="primary" icon="save" htmlType="submit">Add</Button>                                   
                                </Col>
                            </Row>
                        </Form>
                    </Card>
                </Spin>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: get(state, ['question', 'error'], null),
    quizzes: get(state, ['quiz', 'quizzes'], []),
    isLoading: get(state, ['question', 'isLoading'], false),
});

const mapDispatchToProps = dispatch => ({
    quizRequest: () => dispatch(QuizActions.quizRequest()),
    questionSave: (question) => dispatch(QuestionActions.questionSave(question)),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Form.create()(QuestionForm));