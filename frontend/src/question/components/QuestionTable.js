import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Icon, Spin, Table, message, Tag } from 'antd';
import { get, map, pull } from 'lodash';

import QuestionActions from '../redux';
import AnswerForm from '../../answer/components/AnswerForm';



const { Column } = Table;

class QuestionTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            selectedQuestion: {}
        }
    }

    componentDidMount() {
        this.props.initialRequest();     
    }

    componentDidUpdate() {
        const { errorMessage, successMessage } = this.props;
        errorMessage && message.error(errorMessage);
        successMessage && message.success(successMessage);
    }

    renderAnswers = (question) => {
        const { answers } = this.props;
        return answers.filter(a => a.questionId === question.id).map((answer, index) => 
            <Fragment key={index}>
                <Tag color={answer.isCorrect ? 'green' : 'red'} style={{margin: '2.5px'}}>
                    { answer.description }
                </Tag>
                <br />
            </Fragment>
        );
    }

    showAnswerForm = (isVisible, selectedQuestion, update) => {
        update && this.props.initialRequest();
        this.setState({ isVisible, selectedQuestion });
    }


    render() {
        const { questions, isLoading, selectedQuiz, answers } = this.props;
        const { isVisible, selectedQuestion } = this.state;

        let quizSelectedQuestions = [];
        map(questions, question => {
            question.quizId === selectedQuiz.id && quizSelectedQuestions.push(question);
        });        
        
        return(
            <Fragment>
                <Spin spinning={isLoading}>
                    <Table 
                        dataSource={quizSelectedQuestions} 
                        rowKey={question => (question.id)} size="small" 
                        expandedRowRender={(question) => this.renderAnswers(question)}
                    >
                        <Column title="Question" dataIndex="description" key="description" />
                        <Column 
                            title="Actions" 
                            align="right"
                            render={(text, question) => {                                
                                return (
                                    this.renderAnswers(question).length === 0 &&
                                    <Icon 
                                        type="setting" 
                                        onClick={() => this.showAnswerForm(true, question)} 
                                    />
                                );                                               
                            }} 
                        />
                    </Table>
                </Spin>

                <AnswerForm 
                    isVisible={isVisible} 
                    question={selectedQuestion}
                    showForm={this.showAnswerForm} 
                />

            </Fragment>            
        );
    }
}

const mapStateToProps = state => ({
    answers: get(state, ['question', 'answers'], []),
    questions: get(state, ['question', 'questions'], []),
    isLoading: get(state, ['question', 'isLoading'], false),
    errorMessage: get(state, ['question', 'errorMessage'], null),
    successMessage: get(state, ['question', 'successMessage'], null),
});

const mapDispatchToProps = dispatch => ({
    initialRequest: () => dispatch(QuestionActions.questionRequest()),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QuestionTable);