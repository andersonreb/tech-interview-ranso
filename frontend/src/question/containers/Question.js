import React, { Fragment } from 'react';

import QuestionForm from '../components/QuestionForm';
import QuestionTable from '../components/QuestionTable';


export default function Question() {
    return (
        <Fragment>
            <QuestionForm />
            <br />
            <QuestionTable />             
        </Fragment>
    );
};