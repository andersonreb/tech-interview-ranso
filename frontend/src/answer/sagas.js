import { call, put } from 'redux-saga/effects';

import AnswerActions from './redux';
import { api } from '../services/Api';



export function * initialRequest({id}) {
    try {
        const response = yield call(api.getAnswers, id);
        console.log(response.data)
        yield put(AnswerActions.answerSuccess({ answers: response.data }));

    }  catch(e) {
        yield put(AnswerActions.answerFailure({
            error: 'Oh no! Something went wrong while trying to get the answers to the selected question.'
        }));
    }     
}

export function * save({ answer }) { 
    try {
        const response = yield call(api.saveAnswer, answer);
        if(response.status === 201) {
            const resultset = yield call(api.getAnswers);
            yield put(AnswerActions.answerSuccess({ answers: resultset.data }));
        }
    }  catch(e) {
        yield put(AnswerActions.answerFailure({error: 'Oh no! Something went wrong in save.'}));
    }
}

export function * getByQuestionId(id) {
    try {
        const response = yield call(api.getAnswers); 
        yield put(AnswerActions.answerSuccess({ answers: response.data })); 

    } catch(e) {
        yield put(AnswerActions.answerFailure({ error: 'Oh no! Something went wrong in request.' }));
    }
}