import React, { Fragment } from 'react';

import AnswerForm from '../components/AnswerForm';


export default function Answer() {
    return (
        <Fragment>
            <AnswerForm />           
        </Fragment>
    );
};