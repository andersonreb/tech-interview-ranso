import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Input, Spin, Form, Radio, Col, message, Modal, Button, Icon } from 'antd';
import { get, map } from 'lodash';
import uuidv1 from 'uuid/v1';

import AnswerActions from '../redux';



const { Item } = Form;
let id = 0;

class AnswerForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answer: {
                id: null,
                isCorrect: false,
                questionId: null,
                description: null,
            },
            correct: 0
        };
    }

    componentDidMount() {
        this.props.answerRequest();     
    }

    remove = (k) => {
        const { form } = this.props;        
        const keys = form.getFieldValue('keys');
        // Need at least four answers
        if (keys.length === 4) {
            return;
        }
        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    }
    
    add = () => {
        const { form } = this.props;
        const keys = form.getFieldValue('keys');
        // Max of five answers
        if(keys.length < 5) {   
            const nextKeys = keys.concat(id++);
            // Notify form to detect changes
            form.setFieldsValue({
                keys: nextKeys,
            });
        }        
    }
    
    onCorrectChange = (e) => {
        this.setState({ correct: e.target.value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {            
            if(!err) {
                const { question, showForm } = this.props;
                const { correct } = this.state;

                const answers = map(values.answers, (a, idx) => ({
                    id: uuidv1(),
                    description: a,
                    isCorrect: idx === correct,
                    questionId: question.id
                }));
                map(answers, a => this.props.answerSave(a));                
                
                this.props.form.resetFields();
                id = 0;
                showForm(false, null, true);
            }
        });
    }


    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const { isLoading, answers, isVisible, showForm, question } = this.props;
        const { correct } = this.state;

        getFieldDecorator('keys', { initialValue: [] });
        let keys = getFieldValue('keys');
        
        const formItems = (
            <Radio.Group onChange={this.onCorrectChange} value={correct}>
                {keys.map((k, index) => (
                    <Item required={false}  key={k}>
                        
                        <Radio style={{ marginRight: '10px' }} value={k}>{k+1}</Radio>

                        {getFieldDecorator(`answers[${k}]`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [{
                                required: true,
                                whitespace: true,
                                message: "Please input answer's description or delete this field.",
                            }],
                        })(
                            <Input placeholder="Answer description" style={{ width: '75%' }} size="small" />                            
                        )}

                        {keys.length > 4 ? (
                            <Icon
                                className="dynamic-delete-button"
                                type="delete"
                                onClick={() => this.remove(k)}
                                style={{ marginLeft: '10px' }}
                            />
                        ) : null}
                    </Item>
                ))}
            </Radio.Group>
        );

        return (
            <Fragment>
                <Spin spinning={isLoading}>
                    <Form onSubmit={this.handleSubmit}>
                        <Modal
                            visible={isVisible}
                            title={get(question, 'description', null)}
                            onOk={this.handleSubmit}
                            onCancel={() => showForm(false)}
                            footer={[
                                <Button key="back" icon="left" onClick={() => showForm(false)}>Return</Button>,                                
                                <Button 
                                    disabled={keys.length < 4} 
                                    key="submit" 
                                    icon="save" 
                                    type="primary" 
                                    onClick={this.handleSubmit}
                                >
                                    Add
                                </Button>,
                            ]}
                        >
                            { formItems }
                            <Item>
                                <Button 
                                    disabled={keys.length > 4} 
                                    type="dashed" 
                                    onClick={this.add} 
                                    style={{ width: '100%' }}
                                >
                                    <Icon type="plus" /> Add field
                                </Button>
                            </Item>
                            
                        </Modal>
                    </Form>
                </Spin>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    answers: get(state, ['answer', 'answers'], []),
    isLoading: get(state, ['answer', 'isLoading'], false)
});

const mapDispatchToProps = dispatch => ({
    answerSave: (answer) => dispatch(AnswerActions.answerSave(answer)),
    answerRequest: () => dispatch(AnswerActions.answerRequest())
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Form.create()(AnswerForm));