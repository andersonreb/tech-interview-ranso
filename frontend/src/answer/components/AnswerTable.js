import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Spin, Table, message } from 'antd';
import { get } from 'lodash';

import AnswerActions from '../redux';


const { Column } = Table;

class AnswerTable extends Component {
    

    componentDidMount() {
        this.props.initialRequest(this.props.questionId);
    }

    componentDidUpdate() {
        const { error } = this.props;
        error && message.error(error);
    }

    render() {
        const { answers, isLoading } = this.props;        

        return (
            <Fragment>
                <Spin spinning={isLoading}>
                    <Table dataSource={answers} rowKey={record => (record.id)} size="small">
                        <Column title="Answer" dataIndex="answer" key="answer" />
                    </Table>
                </Spin>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: get(state, ['answer', 'error'], null),
    answers: get(state, ['answer', 'answers'], []),
    isLoading: get(state, ['answer', 'isLoading'], false)
});

const mapDispatchToProps = dispatch => ({
    initialRequest: (id) => dispatch(AnswerActions.answerRequest(id)),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AnswerTable);