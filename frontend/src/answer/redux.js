import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { get } from 'lodash';


const { Types, Creators } = createActions({
    answerSave: ['answer'],
    answerRequest: ['id'],
    answerSuccess: ['payload'],
    answerFailure: ['error'],
});

export const AnswerTypes = Types;
export default Creators;


export const INITIAL_STATE = Immutable({
    answer: {},
    answers: [],
    isLoading: false,
    errorMessage: null,
    successMessage: null
});



export const request = (state) => state.merge({
    isLoading: true
});

export const success = (state, { payload }) => state.merge({
    answer: get(payload, ['answer'], state.answer),
    answers: get(payload, ['answers'], []), 
    successMessage: get(payload, ['feedbackMessage'], null),
    isLoading: false,
});

export const failure = (state, { error }) => ({    
    errorMessage: get(error, 'feedbackMessage', null),
    isLoading: false
});



export const reducer = createReducer(INITIAL_STATE, {
    [Types.ANSWER_SAVE]: request,
    [Types.ANSWER_REQUEST]: request,
    [Types.ANSWER_SUCCESS]: success,
    [Types.ANSWER_FAILURE]: failure,
});
