import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { configureStore } from './store/index';
import RouteLayout from './routes';

import Home from './Home';
import Quiz from './quiz/containers/Quiz';
import Question from './question/containers/Question';
import PlayQuiz from './quiz/components/PlayQuiz';
import Answer from './answer/containers/Answer';



const store = configureStore();

function run() {
    render(
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/home" />
                    </Route>
                    <RouteLayout path='/home' component={Home} />
                    <RouteLayout path='/quiz' component={Quiz} />
{/* Delete */}      <RouteLayout path='/answer' component={Answer} />
                    <RouteLayout path='*' component={() => <h2>404 - Not find</h2>} />
                </Switch>
            </Router>
        </Provider>
        , document.getElementById('root')
    );
}

function init() {
    run();
    store.subscribe(run);
}

init();