import React, { Fragment } from 'react';
import { Card } from 'antd';

import QuizTable from './quiz/components/QuizTable';


export default function Home() {
    return (
        <Fragment>
            <Card title="Let's play Quiz">
                <QuizTable />
            </Card>
        </Fragment>
    );
};