import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import QuizForm from '../components/QuizForm';
import QuizTable from '../components/QuizTable';
import QuestionForm from '../../question/components/QuestionForm';
import QuestionTable from '../../question/components/QuestionTable';


// const INITIAL_STATE = 0;
const QUIZ_STATE = 1;
const QUESTION_STATE = 2;
// const PLAY_STATE = 3;

class Quiz extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            currentState: QUIZ_STATE,
            quiz: {}
        }
    }

    isCurrentState = state => {
        return this.state.currentState === state;
    }

    showQuestions = (quiz) => {
        this.setState({ quiz, currentState: QUESTION_STATE });
    }

    showQuiz = () => {
        this.setState({ currentState: QUIZ_STATE });
    }

    render() {
        const { quiz } = this.state;

        return (
            <Fragment>      

                {this.isCurrentState(QUIZ_STATE) &&
                    <Fragment>
                        <QuizForm />
                        <QuizTable displayQuestion={this.showQuestions} />
                    </Fragment>
                }   

                {this.isCurrentState(QUESTION_STATE) &&
                    <Fragment>
                        <QuestionForm selectedQuiz={quiz} showQuiz={this.showQuiz} />
                        <QuestionTable selectedQuiz={quiz} />
                    </Fragment>
                }       
            </Fragment>
        );
    }    
};

export default connect()(Quiz);