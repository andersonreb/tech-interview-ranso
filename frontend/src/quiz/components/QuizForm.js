import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Card, Button, Spin, Input, Form, Row, Col, message } from 'antd';
import { get } from 'lodash';
import uuidv1 from 'uuid/v1';
import { withRouter } from 'react-router-dom';

import QuizActions from '../redux';


const { Item } = Form;

class QuizForm extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialSate();
    }

    getInitialSate() {
        return { quiz: { id: null, theme: null } };
    };

    handleInputChange = (e, property) => {
        let { quiz } = this.state;             
        quiz[property] = e.target.value;
        this.setState({ quiz });
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if(!err) {
                const { quizSave } = this.props;
                
                /**
                 * Setting the id from quiz and preserving its other properties.
                 * In this way, we ensure that we get the current state.
                 */ 
                this.setState(
                   (state) => ({ id: state.quiz.id = uuidv1() }), 
                   () => quizSave(this.state.quiz)
                );
                this.props.form.resetFields();
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { isLoading } = this.props;

        return (
            <Fragment>
                <Spin spinning={isLoading}>
                    <Card title="Add New Quiz">
                        <Form onSubmit={this.handleSubmit}>

                            <Row gutter={16}>
                                <Col>
                                    <Item label="Quiz Theme">
                                        {getFieldDecorator('theme', {
                                            rules: [{ required: true, message: 'Please input the quiz theme!' }],
                                        })(
                                            <Input onChange={e => this.handleInputChange(e, 'theme')} />
                                        )}
                                    </Item>
                                </Col>                                
                            </Row>

                            <Row gutter={16}>
                                <Col>
                                    <Item>
                                        <Button type="primary" icon="save" htmlType="submit">Add</Button>
                                    </Item>
                                </Col>
                            </Row>

                        </Form>
                    </Card>
                </Spin>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    isLoading: get(state, ['quiz', 'isLoading'], false)
});

const mapDispatchToProps = dispatch => ({
    quizSave: (quiz) => dispatch(QuizActions.quizSave(quiz)),
});


export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Form.create()(QuizForm)));