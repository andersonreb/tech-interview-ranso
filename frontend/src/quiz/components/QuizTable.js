import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Table, message, Divider } from 'antd';
import { get, isNil } from 'lodash';

import QuizActions from '../redux';


const { Column } = Table;
const activePlay = false;

class QuizTable extends Component {

    constructor(props) {
        super(props);
        this.state = activePlay;
    }

    componentDidMount() {
        this.props.initialRequest();     
    }

    componentDidUpdate() {
        const { errorMessage, successMessage } = this.props;
        errorMessage && message.error(errorMessage);
        successMessage && message.success(successMessage);
    }
    

    render() {
        const { quizzes, displayQuestion } = this.props;
        
        return(
            <Fragment>
                
                    <Table dataSource={quizzes} rowKey={record => record.id} size="small">
                        <Column title="Quizzes" dataIndex="theme" key="theme" />
                        <Column 
                            title="Actions" 
                            align="right"
                            render={(text, record) => {                                
                                return (
                                    !isNil(displayQuestion) ?
                                        <Fragment>
                                            <a onClick={() => displayQuestion(record)}>Add question</a>
                                        </Fragment>
                                    : 
                                        <Fragment>
                                            <a onClick={() => null}>Play it</a>
                                        </Fragment>
                                );                                               
                            }} 
                        />
                    </Table>                   
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
        error: get(state, ['quiz', 'error'], null),
        quizzes: get(state, ['quiz', 'quizzes'], []),
        isLoading: get(state, ['quiz', 'isLoading'], false),
        errorMessage: get(state, ['quiz', 'errorMessage'], null),
        successMessage: get(state, ['quiz', 'successMessage'], null),
});

const mapDispatchToProps = dispatch => ({
    initialRequest: () => dispatch(QuizActions.quizRequest()),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QuizTable);