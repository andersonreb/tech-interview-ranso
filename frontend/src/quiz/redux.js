import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { get } from 'lodash';


const { Types, Creators } = createActions({
    quizRequest: null,
    quizSave: ['quiz'],
    quizSuccess: ['payload'],
    quizFailure: ['error'],
});

export const QuizTypes = Types;
export default Creators;


export const INITIAL_STATE = Immutable({
    quiz: {},
    quizzes: [],
    isLoading: false,
    errorMessage: null,
    successMessage: null
});


export const request = (state) => state.merge({
    isLoading: true
});

export const success = (state, action) => state.merge({
    quiz: get(action.payload, ['quiz'], state.quiz),
    quizzes: get(action.payload, ['quizzes'], state.quizzes), 
    successMessage: get(action.payload, ['feedbackMessage'], null),
    isLoading: false
});

export const failure = (state, { error }) => ({    
    errorMessage: get(error, 'feedbackMessage', null),
    isLoading: false
});



export const reducer = createReducer(INITIAL_STATE, {
    [Types.QUIZ_REQUEST]: request,
    [Types.QUIZ_SAVE]: request,
    [Types.QUIZ_SUCCESS]: success,
    [Types.QUIZ_FAILURE]: failure,
});
