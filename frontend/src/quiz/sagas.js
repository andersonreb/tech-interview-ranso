import { call, put } from 'redux-saga/effects';

import QuizActions from './redux';
import { api } from '../services/Api';



export function * initialRequest() { 
    try {
        const response = yield call(api.getQuizzes); 
        yield put(QuizActions.quizSuccess({ quizzes: response.data })); 

    } catch(e) {
        yield put(QuizActions.quizFailure({ feedbackMessage: 'Oh no! Something went wrong in request.' }));
    }       
}


export function * save({ quiz }) {  
    try {
        const response = yield call(api.saveQuiz, quiz);
        if(response.status === 201) {
            const resultset = yield call(api.getQuizzes);
            yield put(QuizActions.quizSuccess({ 
                quizzes: resultset.data,
                feedbackMessage: "Successful saved!" 
            }));
        }
    }  catch(e) {
        yield put(QuizActions.quizFailure({ feedbackMessage: 'Oh no! Something went wrong in save.' }));
    }
}