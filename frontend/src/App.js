import React, { Fragment } from 'react';
import { Layout, Card } from 'antd';
import Header from './components/Header';



const { Content, Footer } = Layout;

const App = ({ children }) => {
    
    return (
        <Fragment>
            <Layout className="layout">
                <Header />

                <Content>
                    <Card>
                        <div style={{ background: '#fff', padding: 10, minHeight: 280 }}>
                            { children }
                        </div>
                    </Card>
                </Content>

                <Footer style={{ textAlign: 'center' }}>
                    Ranso ©2019 
                </Footer>
            </Layout>
        </Fragment>
    );
}


export default App;
