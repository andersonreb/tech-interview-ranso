import React from 'react';
import { Layout } from 'antd';

import Navigator from './Navigator';

const Header = () => {
    return (
        <Layout.Header>
            <div className="logo">
                {/* <h1 style={{color: '#FFF'}}>QUIZ</h1> */}
            </div>
            <Navigator />
        </Layout.Header>
    );
}

export default Header;

