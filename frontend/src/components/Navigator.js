import React from 'react';
import { Link } from 'react-router-dom'
import { Menu } from 'antd';        
        

const { Item } = Menu;

const Navigator = () => {
    return (
        <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['1']}
            style={{ lineHeight: '64px' }}
        >
            <Item key="1">
                <Link to="/home">Play</Link>
            </Item>

            <Item key="2">
                <Link to="/quiz">Manager</Link>
            </Item>
        </Menu>
    );
}


export default Navigator;