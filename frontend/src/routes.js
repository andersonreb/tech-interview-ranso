import React from 'react';
import { Route } from 'react-router-dom';

import App from './App';



const RouteLayout = ({ component: Component, ...rest }) => {

    return (
        <Route {...rest} render={matchProps => (
            <App>
                <Component {...matchProps} />
            </App>
        )} />
    );
}


export default RouteLayout;
